*******  MODLET MUST BE ON BOTH CLIENT AND THE SERVER
TO USE THIS MOD IN MULTIPLAYER

V1 version is not backward compatible with A21 *******


The 'Working TFP Raft' .  This again was made in A17.4 but not published on this site until A19 came out . The TFP raft model was extracted and rigged initially
as a 'working vehicle entity' by DUST2DEATH and then 'Raft float xml physics' applied by myself.

 
Launch
The Raft can be placed in shallow water with level ground underneath
much the same as the latest version of the 'boating modlet'.
Once in the water level the Raft and off you go.

Controls
Normal keys W and Shift for forward momentum and Space bar and C
control the up and down angles (same as a gyro) which you may need to
adjust slightly as you travel on water. Although once levelled the Raft
is much more stable than a boat. Steer the Raft with the A and D keys.
 
If you let go of W and shift (Turbo) then the Raft slowly comes to a
stop, there is no reverse on a raft although pressing S will slow the
Raft down more quickly this replaces Spacebar as the brake key.
 
To 'anchor the Raft' and stop it drifting away use C and Spacebar to
level the Raft until it stops drifting, useful if you have a fishing mod
installed.
 
Crafting
The Raft is craftable in a standard workbench with no progression
needed at the cost of having enough resources and actually finding a
working workbench. Finding a workbench in early game would be a bonus
and allow you to make the Raft a lot sooner.
 
The recipe's are simple and use standard in game resources... feel free to alter the recipe if you wish
and offer backa recipe or build system improvement with due credits given..
 
The Raft has 4 seating positions ... two can be added via the vehicle mod system 
let me know if there are any issues with multiple passengers.

ChangeLog version 4:
1. XML Updated to V1 
2. Seating redone for V1 Pose system
3. Texture pass for V1 world lighting.
4. Physics update for V1 Series


Additional Credits : DUST2DEATH for the extracting the model and separating the sail for conversion in unity. TFP for the 'Actual Raft Model' asset in the game.

7 Days to Die Forum community for wanting water based vehicles.
Guppy's Modding Discord for their support along the way.