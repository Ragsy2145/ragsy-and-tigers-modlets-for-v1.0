
Brand New Experimental Physics 'Work in progress'  for version 3 of the JetSki

(There were no updates that worked well enough in A21 for a release but
the JetSki returns in V1 Series with major physics updates and has some new options).


Launch

Place the JetSki on/in water in a 'shallow water' area that has fairly flat ground underneath, if you have no shallow water in your map then
build a small launch dock using standard blocks, add some fuel to the JetSki and off you go.

The JetSki will not work on land (It may move very very slightly due to Motor torque but will have no steering or control over direction)


Controls
Standard WASD controls for this version as it is based on the motorcycle and the JetSki can now be steered with the mouse like a standard vehicle.

The new JetSki can 'hop' using the same functions as the motorcycle to avoid any obstacles that may be in the water and is also very hard to sink it.    

S will act as brake and spacebar will stop the JetSki quicker.


Crafting
The JetSki is craftable in a 'standard workbench' with no progression needed at the heavier cost of resources by creating the required assemblies to build it of which some parts are from a standard motorcycle (Handlebars and Chassis).


Other information
The 'actual top speed' wont show the real speed in the vehicles display for some reason in V1.1 but a manual check was done to ensure the boat has enough speed without exceeding the games limits.  The JetSki can be picked up into inventory, if you crash it at any point, although be careful if you crash or get stuck too far away from the shores as it could be a long swim back or to find an area where it can be relaunched.
 
Best to not store stuff in a boat or Jetski as Zombies can attack water vehicles in game.



Changelog: Version 3
1. Adapted to V1.1 XML system  
2. Brand new physics used based on the Motorcycle giving option for JetSki to be able to 'Hop.  
3. Jetski will not work on land (will move very slightly but no steering or control over direction)


Changelog: Version 2
1. Adapted to A20 XML including some physics changes  
2. Resized Jetski and adjusted seat positions to match  - may add option later for modding in the second seat at the moment has two 'fixed' seats.
3. Vehicle Mods enabled - Fuel Saver and reserve tank , dye useable and will tint the flash trim only, rest of jetski has a custom texture.


Additional Credits:
Textures by ActiniumTiger 

Happy Boating in V1.1

Ragsy and Tiger !!


 

